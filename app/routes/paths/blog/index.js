
/**
 * Created by anuj on 10/8/17.
 */



/*
 |--------------------------------------------------------------------------
 | Blog APIs'
 |--------------------------------------------------------------------------
 |
 | All APIs related to Blog are here
 |
 */


module.exports = function(app, requestInputs, response) {

    app.post('/api/blog/create', function (req, res, next) {

        console.log(" hit create blog api");
        app.api.blogs.createBlog(requestInputs(req), function (apiResponse) {
            response(apiResponse, res);
        });
    });


    app.get('/api/blog/list/', function (req, res, next) {

        console.log(" hit getList blog api");
        app.api.blogs.getBlogs(requestInputs(req), function (apiResponse) {
            response(apiResponse, res);
        });
    });

    app.get('/api/blog/:id', function (req, res, next) {

        console.log(" hit get blog with comments api");
        app.api.blogs.getBlogWithComments(requestInputs(req), function (apiResponse) {
            response(apiResponse, res);
        });
    });

    app.post('/api/blog/addComments', function (req, res, next) {

        console.log(" hit the add comments api");
        app.api.blogs.addCommentOnPara(requestInputs(req), function (apiResponse) {
            response(apiResponse, res);
        });
    });


}