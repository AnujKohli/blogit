
/**
 * Created by anuj on 10/8/17.
 */

    var blogRoute             =   require("./paths/blog");
    var routeHelper           =   require("../helpers/routeHelper");

    /**
     * function to initialise route path for blog
     *@param app
     */

    function routeInit (app, requestInputs, response)
    {
        blogRoute(app, requestInputs, response);
    }


    module.exports = function(app) {

        var requestInputs = routeHelper.requestInputs;
        var response = routeHelper.response;

        routeInit(app, routeHelper.requestInputs, routeHelper.response);

    }


