/**
 * Created by anuj on 10/8/17.
 */

var blogHelper       = require("./blog/index")


/*_________________________________________________________________
 *
 * Create a new blog post
 * Get a list of blogs per page
 * Get comments of a particular blog post
 * add/update comments on the para of a blog post
 *__________________________________________________________________
 * */

/**
 *Create a new blog post
 * @param params
 * @param cb : callback
 */
exports.createBlog = function (params,cb) {

    blogHelper.createBlog(params, function(response) {
        cb(response);
    });
}

/**
 * Get a list of blogs per page
 * @param params
 * @param cb : callback
 */
exports.getBlogs = function (params,cb) {

    blogHelper.getBlogs(params, function(response) {
        cb(response);
    });

}

/**
 * Get comments of a particular blog post
 * @param params
 * @param cb : callback
 */
exports.getBlogWithComments = function (params,cb) {

    blogHelper.getBlogWithComments(params, function(response) {
        cb(response);
    });

}


/**
 * add/update comments on the para of a blog post
 * @param params
 * @param cb : callback
 */
exports.addCommentOnPara = function (params,cb) {

    blogHelper.addCommentOnPara(params, function(response) {
        cb(response);
    });

}
