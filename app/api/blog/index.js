
/**
 * Created by anuj on 10/8/17.
 */


var mongoose        = require('mongoose');
var Blog            = mongoose.model('Blog');
var response        = require("../../helpers/responseHelper").response;


/*_________________________________________________________________
 *
 * Create a new blog post
 * Get a list of blogs per page
 * Get comments of a particular blog post
 * add/update comments on the para of a blog post
 *__________________________________________________________________
 * */


/**
 *Create a new blog post
 * @param params
 * @param cb : callback
 */
exports.createBlog = function (params,cb) {

    let {title, content}                     = params.input;
    let responseObj                          = params.constants.RESPONSE_OBJ;


    paraArray = content.split("\n\n");

    blogObj = new Blog({blogTitle: title});

    paraArray.forEach((paraContent, index) => {

        let para = {};
        para.paraId = index;
        para.content = paraContent;

        blogObj.paragraphs.push(para)

    });


    blogObj.save()
        .then ((blogPost) => {
            cb(response(params.constants.STATUS_SUCCESS, "Blogs created successfully", blogPost));
        })
        .catch(err => {
            cb(response (params.constants.STATUS_FAILURE, "Error in blog creation, please check the error",[], err));
        })

}


/**
 * Get a list of blogs per page
 * @param params
 * @param cb : callback
 */
exports.getBlogs = function (params, cb){

    let {pageId, limit = 5} = params.query;

    limit = parseInt(limit);

    let skip = parseInt(pageId)*limit;

    let blogsServed = skip + limit;


    Promise.all([

        Blog.find({}, {}, {limit: limit, skip: skip}).select('-comments').sort({createdDate: -1}).exec(),
        Blog.count().exec()
    ])
        .then(([blogs, totalCount]) => {
            const hasNext = totalCount > blogsServed;

            if (blogs.length) {

                var responseOb = {};
                responseOb.status = params.constants.STATUS_SUCCESS;
                responseOb.msg = "blogs list displayed successfully";
                responseOb.data = blogs;
                responseOb.hasNext = hasNext;
                cb(responseOb);

            }

            else {
                cb(response(params.constants.STATUS_SUCCESS, "No blogs found", [], ""));
            }


        })
        .catch(err => {
            cb( params.constants.STATUS_FAILURE, "Error while retreiving blogs",[], err);
        })

}

/**
 * Get comments of a particular blog post
 * @param params
 * @param cb : callback
 */
exports.getBlogWithComments = function(params, cb){

    let {id} = params.params;

    Blog.findOne({_id: id}).exec()
        .then(function(blog){

            if (blog){
                cb(response(params.constants.STATUS_SUCCESS, "Blog retreived successfully", blog));
            }
            else {
                cb(response(params.constants.STATUS_FAILURE, "no corresponding blog found.", [], ""));
            }
        })
        .catch(function (err){
            cb(response(params.constants.STATUS_FAILURE, "Error in the blog retreival, please check the error", [], err));
        })

}


/**
 * add/update comments on the para of a blog post
 * @param params
 * @param cb : callback
 */
exports.addCommentOnPara = function(params, cb){

    let {blogId, paraId, comment} = params.input;


    Blog.findOne({_id: blogId}).exec()
        .then(function(blog){


            if (blog){

               let comments = blog.comments;

                if (comments && comments.length >0) {
                    let commentPresent = false;

                    comments = comments.map(eachComment => {
                        if (eachComment.paraId == parseInt(paraId)) {
                            eachComment.content = comment;
                            commentPresent = true;
                            return eachComment;
                        }
                        else {
                            return eachComment;
                        }
                    })

                    if (!commentPresent) {
                        blog.comments.push({paraId: paraId, content: comment})
                    }
                }
                else {
                    blog.comments.push({paraId: paraId, content: comment})
                }

                blog.modifiedAt = new Date();

                blog.save()
                    .then((updatedBlog) =>{
                        cb(response(params.constants.STATUS_SUCCESS, "Comment successfully added for a blog's para ", updatedBlog));
                    })

            }
            else {
               return cb(response(params.constants.STATUS_FAILURE, "Could not retreive the corresponding blog", [], ""));
            }
        })
        .catch(function (err){
            cb(response(params.constants.STATUS_FAILURE, "Error in the blog comment updation, please check the error", [], err));
        })

}


