/**
 * Created by anuj on 10/8/17.
 */


/*______________________________________________________________
 *
 * Response api Helper function
 *______________________________________________________________

 * */


/**
 *
 * @param status
 * @param message
 * @param data
 * @param err
 * @returns {*} responseObj
 */

 exports.response = function( status = 1, message ="", data = [], err = "" ){

 let responseObj = {};
 responseObj.status = status;
 responseObj.msg    = message
 responseObj.data   = data;
 responseObj.error  = err;
 return responseObj;

 }