/**
 * Created by anuj on 10/8/17.
 */


/**
 *  Method to extract all input elements
 * @param req
 * @param res
 */
exports.requestInputs = function (req) {

    var params          =   {};
    params.input        =   req.body;
    params.query        =   req.query,
    params.headers      =   req.headers;
    params.params       =   req.params;
    params.constants    =   req.app.settings.constants;
    return params;
}


/**
 * Method to send response for a request
 * @param response
 * @param res
 */
exports.response = function (response, res) {

   if (response.status == "0")
   {
       res.status(400);
   }

    res.send(response);

}
