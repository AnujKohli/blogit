/**
 * Created by anuj on 10/8/17.
 */


var mongoose            = require('mongoose');
var Schema              = mongoose.Schema;

module.exports  = function ()
{
    var ParaSchema  = new Schema({

        paraId  : { type: Number, required: true},
        content : { type: String, required: true}
    });


    var CommentSchema  = new Schema({

        paraId  : { type: Number, required: true},
        content : { type: String, required: true}
    });


    var BlogSchema  = new Schema({

        blogTitle  : { type: String, required: true},
        createdAt  : { type: Date,   default: Date.now},
        modifiedAt : { type: Date,   default: Date.now},
        paragraphs : [ParaSchema],
        comments   : [CommentSchema]

    });

    mongoose.model("Blog", BlogSchema, "blogs");
}
