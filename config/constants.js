
/**
 * In ECMAScript 6 const is defined (This is browser dependant)
 * Here we are defining object value with non editable value
 * @param name
 * @param value
 */
function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        writable:   false,
        enumerable: true
    });
}


/*
 |--------------------------------------------------------------------------
 | constants defined below
 |--------------------------------------------------------------------------
 |
 | Defining a constant :
 |
 |   define("CONSTANT_NAME", "CONSTANT_VALUE");
 |
 | Usage :
 |   constants = require('./constants');
 |   console.log(constants.CONSTANT_NAME);
 |
 |   - from app settings
 |   console.log(app.settings.constants.CONSTANT_NAME);
 |
 */


// =========== Status Contants ============
define("STATUS_SUCCESS", "1");
define("STATUS_FAILURE", "0");

define("RESPONSE_OBJ", {status: "", msg: "", data:[], error: "" });
