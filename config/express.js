var express             = require('express');
var load                = require('express-load');
var bodyParser          = require('body-parser');
var path                = require ('path');
var app                 = express();
var server              = require('http').Server(app);
var constants           = require('./constants');
var mongodb             = require('./database');


module.exports = function() {

    //Set port to 8081
    app.set('port', 8081);

    //Use the public folder for static files
    app.use(express.static(path.join(__dirname + './../app/views/')));

    // to support JSON-encoded bodies
    app.use( bodyParser.json() );

    // to support URL-encoded bodies
    app.use(bodyParser.urlencoded({extended : true}));

    //configure mvc
    load('models', {cwd:'app'})
    .then('api')
    .into(app);


    // Set constants in app
    app.set("constants", constants);


    // Now load routes
    load('routes', {cwd: 'app'}).into(app);

    var webserver           = [];
    webserver["app"    ]    = app;
    webserver["server" ]    = server;
    webserver["port"   ]    = 8081 ;

    return webserver;
};
