// Get express webserver instance
const webserver = require('./config/express')();

// Listen App Server
webserver.server.listen(webserver.port);

// Log webserver run
console.log('BlogIt listening at port ' + webserver.port);