
How to run
-----------------

After cloning the project run
>  npm install

>  node server.js

> open http://localhost:8081/api/docs/ for swagger documentation. It contains api documentations and you could execute the apis in-place.

MongoDb is hosted on MongoDB Atlas, and connection is estabilished in the code and will be connected when server is up. Nothing else has to be done regarding it.

Code is properly structured into various modules and files. Doc and function comments are provided for the ease of the viewer,


